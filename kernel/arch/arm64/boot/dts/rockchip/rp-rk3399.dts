/*
 * Copyright (c) 2017 Fuzhou Rockchip Electronics Co., Ltd
 *
 * This file is dual-licensed: you can use it either under the terms
 * of the GPL or the X11 license, at your option. Note that this dual
 * licensing only applies to this file, and not this project as a
 * whole.
 *
 *  a) This file is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation; either version 2 of the
 *     License, or (at your option) any later version.
 *
 *     This file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 * Or, alternatively,
 *
 *  b) Permission is hereby granted, free of charge, to any person
 *     obtaining a copy of this software and associated documentation
 *     files (the "Software"), to deal in the Software without
 *     restriction, including without limitation the rights to use,
 *     copy, modify, merge, publish, distribute, sublicense, and/or
 *     sell copies of the Software, and to permit persons to whom the
 *     Software is furnished to do so, subject to the following
 *     conditions:
 *
 *     The above copyright notice and this permission notice shall be
 *     included in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *     OTHER DEALINGS IN THE SOFTWARE.
 */

/dts-v1/;

#include "dt-bindings/pwm/pwm.h"
#include "rk3399.dtsi"
#include "rk3399-opp.dtsi"
#include <dt-bindings/display/drm_mipi_dsi.h>
#include <dt-bindings/display/media-bus-format.h>
#include "rk3399-vop-clk-set.dtsi"

//#include "rp_lcd_hdmi.dtsi"
//#include "rp_lcd_hdmi-270.dtsi"
//#include "rp_lcd_mipi_7_800_1280.dtsi"
//#include "rp_lcd_mipi_720_1280_old.dtsi"
//#include "rp_lcd_mipi_5_720_1280.dtsi"
//#include "rp_lcd_mipi_1920_1200.dtsi"
//#include "rp_lcd_edp_1920_1080.dtsi"
//#include "rp_lcd_mipi_10.1_1920_1200_I2S1.dtsi"
#include "rp_lcd_mipi_7_1200_1920.dtsi"
//#include "rp_lcd_mipi_8_800_1280.dtsi"
//#include "rp_lcd_mipi_7_1024-600.dtsi"
//#include "rp_lcd_hdmi-4k.dtsi"
//#include "rp_lcd_edp_1920_1080_mipi_10_1920_1200.dtsi"
//#include "rp_lcd_mipi_5.5_720_1280.dtsi"
//#include "rp_lcd_mipi_4_480_800.dtsi"

//#include "rp_lcd_mipi_7_new_800_1280.dtsi"

//atk
//#include "rp_lcd_hdmi-atk.dtsi"


/ {
	compatible = "rockchip,android", "rockchip,rk3399";

	chosen {
		bootargs = "earlycon=uart8250,mmio32,0xff1a0000 swiotlb=1 coherent_pool=1m";
	};


	ramoops_mem: ramoops_mem {
		reg = <0x0 0x110000 0x0 0xf0000>;
		reg-names = "ramoops_mem";
	};

	ramoops {
		compatible = "ramoops";
		record-size = <0x0 0x20000>;
		console-size = <0x0 0x80000>;
		ftrace-size = <0x0 0x00000>;
		pmsg-size = <0x0 0x50000>;
		memory-region = <&ramoops_mem>;
	};


	reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		drm_logo: drm-logo@00000000 {
			compatible = "rockchip,drm-logo";
			reg = <0x0 0x0 0x0 0x0>;
		};

		secure_memory: secure-memory@20000000 {
			compatible = "rockchip,secure-memory";
			reg = <0x0 0x20000000 0x0 0x10000000>;
			status = "disabled";
		};

		stb_devinfo: stb-devinfo@00000000 {
			compatible = "rockchip,stb-devinfo";
			reg = <0x0 0x0 0x0 0x0>;
		};
	};


	clkin_gmac: external-gmac-clock {
		compatible = "fixed-clock";
		clock-frequency = <125000000>;
		clock-output-names = "clkin_gmac";
		#clock-cells = <0>;
	};


	iram: sram@ff8d0000 {
		compatible = "mmio-sram";
		reg = <0x0 0xff8d0000 0x0 0x20000>; /* 128k */
	};


	rga: rga@ff680000 {
		compatible = "rockchip,rga2";
		dev_mode = <1>;
		reg = <0x0 0xff680000 0x0 0x1000>;
		interrupts = <GIC_SPI 55 IRQ_TYPE_LEVEL_HIGH 0>;
		clocks = <&cru ACLK_RGA>, <&cru HCLK_RGA>, <&cru SCLK_RGA_CORE>;
		clock-names = "aclk_rga", "hclk_rga", "clk_rga";
		power-domains = <&power RK3399_PD_RGA>;
		dma-coherent;
		status = "okay";
	};



	sdio_pwrseq: sdio-pwrseq {
		compatible = "mmc-pwrseq-simple";
		clocks = <&rk808 1>;
		clock-names = "ext_clock";
		pinctrl-names = "default";
		pinctrl-0 = <&wifi_enable_h>;

		/*
		 * On the module itself this is one of these (depending
		 * on the actual card populated):
		 * - SDIO_RESET_L_WL_REG_ON
		 * - PDN (power down when low)
		 */
		reset-gpios = <&gpio0 10 GPIO_ACTIVE_LOW>; /* GPIO0_B2 */
	};

	vcc3v3_pcie: vcc3v3-pcie-regulator {
		compatible = "regulator-fixed";
		enable-active-high;
		regulator-always-on;
		regulator-boot-on;
		gpio = <&gpio1 20 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&pcie_drv>;
		regulator-name = "vcc3v3_pcie";
	};
	
	vcc3v3_sys: vcc3v3-sys {
		compatible = "regulator-fixed";
		regulator-name = "vcc3v3_sys";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
	};
	
	firmware {
		android {
			compatible = "android,firmware";
			fstab {
				compatible = "android,fstab";
				system {
					compatible = "android,system";
					dev = "/dev/block/by-name/system";
					type = "ext4";
					mnt_flags = "rw,barrier=1,inode_readahead_blks=8";
					//fsmgr_flags = "wait,verify";
					fsmgr_flags = "wait";
				};
				vendor {
					compatible = "android,vendor";
					dev = "/dev/block/by-name/vendor";
					type = "ext4";
					mnt_flags = "rw,barrier=1,inode_readahead_blks=8";
					//fsmgr_flags = "wait,verify";
					fsmgr_flags = "wait";
				};
			};
		};
	};
	
	

	vcc5v0_sys: vcc5v0-sys {
		compatible = "regulator-fixed";
		regulator-name = "vcc5v0_sys";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
	};

//bill no
	vcc_sd: vcc-sd {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio0 1 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&vcc_sd_h>;
		regulator-name = "vcc_sd";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
	};

	vcc_phy: vcc-phy-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_phy";
		regulator-always-on;
		regulator-boot-on;
	};

	vdd_log: vdd-log {
		compatible = "pwm-regulator";
		pwms = <&pwm2 0 25000 1>;
		regulator-name = "vdd_log";
		regulator-min-microvolt = <800000>;
		regulator-max-microvolt = <1400000>;
		regulator-always-on;
		regulator-boot-on;

		/* for rockchip boot on */
		rockchip,pwm_id= <2>;
		rockchip,pwm_voltage = <1000000>;
	};

	wireless-wlan {
		compatible = "wlan-platdata";
		rockchip,grf = <&grf>;
		wifi_chip_type = "ap6356";
		sdio_vref = <1800>;
		WIFI,host_wake_irq = <&gpio0 3 GPIO_ACTIVE_HIGH>; // GPIO0_a3 
		status = "okay";
	};

	wireless-bluetooth {
		compatible = "bluetooth-platdata";
		clocks = <&rk808 1>;
		clock-names = "ext_clock";
		//wifi-bt-power-toggle;
		uart_rts_gpios = <&gpio2 19 GPIO_ACTIVE_LOW>; // GPIO2_C3 
		pinctrl-names = "default", "rts_gpio";
		pinctrl-0 = <&uart0_rts>;
		pinctrl-1 = <&uart0_gpios>;
		//BT,power_gpio  = <&gpio3 19 GPIO_ACTIVE_HIGH>; // GPIOx_xx 
		BT,reset_gpio    = <&gpio0 9 GPIO_ACTIVE_HIGH>; // GPIO0_B1 
		BT,wake_gpio     = <&gpio0 12 GPIO_ACTIVE_HIGH>; // GPIO0_B4 
		BT,wake_host_irq = <&gpio0 4 GPIO_ACTIVE_HIGH>; // GPIO0_A4 
		status = "okay";
	};

};


&dmac_bus {
	iram = <&iram>;
	rockchip,force-iram;
};
&dfi {
	status = "okay";
};

#if 0
&dmc {
	status = "okay";
	center-supply = <&vdd_center>;
	upthreshold = <40>;
	downdifferential = <20>;
	system-status-freq = <
		/*system status         freq(KHz)*/
		SYS_STATUS_NORMAL       800000
		SYS_STATUS_REBOOT       528000
		SYS_STATUS_SUSPEND      200000
		SYS_STATUS_VIDEO_1080P  200000
		SYS_STATUS_VIDEO_4K     600000
		SYS_STATUS_VIDEO_4K_10B 800000
		SYS_STATUS_PERFORMANCE  800000
		SYS_STATUS_BOOST        400000
		SYS_STATUS_DUALVIEW     600000
		SYS_STATUS_ISP          600000
	>;
	vop-bw-dmc-freq = <
	/* min_bw(MB/s) max_bw(MB/s) freq(KHz) */
		0       577      200000
		578     1701     300000
		1702    99999    400000
	>;
	auto-min-freq = <200000>;
};
#else
#if 0
&dmc {
	status = "okay";
	center-supply = <&vdd_center>;
	system-status-freq = <
		//system status         freq(KHz)
		SYS_STATUS_NORMAL       800000
		SYS_STATUS_REBOOT       400000
		SYS_STATUS_SUSPEND      400000
		SYS_STATUS_VIDEO_1080P  800000
		SYS_STATUS_VIDEO_4K     800000
		SYS_STATUS_VIDEO_4K_10B 800000
		SYS_STATUS_PERFORMANCE  800000
		SYS_STATUS_BOOST        400000
		SYS_STATUS_DUALVIEW     800000
		SYS_STATUS_ISP          800000
	>;
	auto-min-freq = <400000>;
	auto-freq-en = <0>;
};
#endif
#if 1
&dmc {
	status = "okay";
	center-supply = <&vdd_center>;
	system-status-freq = <
		//system status         freq(KHz)
		SYS_STATUS_NORMAL       800000
		SYS_STATUS_REBOOT       800000
		SYS_STATUS_SUSPEND      800000
		SYS_STATUS_VIDEO_1080P  800000
		SYS_STATUS_VIDEO_4K     800000
		SYS_STATUS_VIDEO_4K_10B 800000
		SYS_STATUS_PERFORMANCE  800000
		SYS_STATUS_BOOST        800000
		SYS_STATUS_DUALVIEW     800000
		SYS_STATUS_ISP          800000
	>;
	auto-min-freq = <800000>;
	auto-freq-en = <0>;
};
#endif
#endif

&cpu_l0 {
	cpu-supply = <&vdd_cpu_l>;
};

&cpu_l1 {
	cpu-supply = <&vdd_cpu_l>;
};

&cpu_l2 {
	cpu-supply = <&vdd_cpu_l>;
};

&cpu_l3 {
	cpu-supply = <&vdd_cpu_l>;
};

&cpu_b0 {
	cpu-supply = <&vdd_cpu_b>;
};

&cpu_b1 {
	cpu-supply = <&vdd_cpu_b>;
};



&emmc_phy {
	status = "okay";
};

&gmac {
	phy-supply = <&vcc_phy>;
	phy-mode = "rgmii";
	clock_in_out = "input";
	snps,reset-gpio = <&gpio3 15 GPIO_ACTIVE_LOW>;
	snps,reset-active-low;
	snps,reset-delays-us = <0 10000 50000>;
	assigned-clocks = <&cru SCLK_RMII_SRC>;
	assigned-clock-parents = <&clkin_gmac>;
	pinctrl-names = "default";
	pinctrl-0 = <&rgmii_pins>;
	tx_delay = <0x28>;
	rx_delay = <0x11>;
	status = "okay";
};


&gpu {
	status = "okay";
	mali-supply = <&vdd_gpu>;
};


&i2c0 {
	status = "okay";
	i2c-scl-rising-time-ns = <168>;
	i2c-scl-falling-time-ns = <4>;
	clock-frequency = <400000>;

	vdd_cpu_b: syr827@40 {
		compatible = "silergy,syr827";
		reg = <0x40>;
		vin-supply = <&vcc5v0_sys>;
		regulator-compatible = "fan53555-reg";
		pinctrl-names = "default";
		pinctrl-0 = <&vsel1_gpio>;
		vsel-gpios = <&gpio1 17 GPIO_ACTIVE_HIGH>;
		regulator-name = "vdd_cpu_b";
		regulator-min-microvolt = <712500>;
		regulator-max-microvolt = <1500000>;
		regulator-ramp-delay = <1000>;
		fcs,suspend-voltage-selector = <1>;
		regulator-always-on;
		regulator-boot-on;
		regulator-initial-state = <3>;
			regulator-state-mem {
			regulator-off-in-suspend;
		};
	};

	vdd_gpu: syr828@41 {
		compatible = "silergy,syr828";
		reg = <0x41>;
		vin-supply = <&vcc5v0_sys>;
		regulator-compatible = "fan53555-reg";
		pinctrl-names = "default";
		pinctrl-0 = <&vsel2_gpio>;
		vsel-gpios = <&gpio1 14 GPIO_ACTIVE_HIGH>;
		regulator-name = "vdd_gpu";
		regulator-min-microvolt = <712500>;
		regulator-max-microvolt = <1500000>;
		regulator-ramp-delay = <1000>;
		fcs,suspend-voltage-selector = <1>;
		regulator-always-on;
		regulator-boot-on;
		regulator-initial-state = <3>;
			regulator-state-mem {
			regulator-off-in-suspend;
		};
	};

	rk808: pmic@1b {
		compatible = "rockchip,rk808";
		reg = <0x1b>;
		interrupt-parent = <&gpio1>;
		interrupts = <21 IRQ_TYPE_LEVEL_LOW>;
		
		pinctrl-names = "default";
		pinctrl-0 = <&pmic_int_l &pmic_dvs2>;
		rockchip,system-power-controller;
		wakeup-source;
		#clock-cells = <1>;
		clock-output-names = "xin32k", "rk808-clkout2";

		vcc1-supply = <&vcc5v0_sys>;
		vcc2-supply = <&vcc5v0_sys>;
		vcc3-supply = <&vcc5v0_sys>;
		vcc4-supply = <&vcc5v0_sys>;
		vcca-supply = <&vcc5v0_sys>;
		vcc6-supply = <&vcc5v0_sys>;
		vcc7-supply = <&vcc5v0_sys>;
		vcc8-supply = <&vcc3v3_sys>;
		vcc9-supply = <&vcc5v0_sys>;
		vcc10-supply = <&vcc5v0_sys>;
		vcc11-supply = <&vcc5v0_sys>;
		vcc12-supply = <&vcc3v3_sys>;
		vddio-supply = <&vcc1v8_pmu>;

		regulators {
			vdd_center: DCDC_REG1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <750000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <6001>;
				regulator-name = "vdd_center";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vdd_cpu_l: DCDC_REG2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <750000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <6001>;
				regulator-name = "vdd_cpu_l";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_ddr: DCDC_REG3 {
				regulator-always-on;
				regulator-boot-on;
				regulator-name = "vcc_ddr";
				regulator-state-mem {
					regulator-on-in-suspend;
				};
			};

			vcc_1v8: DCDC_REG4 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcc_1v8";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1800000>;
				};
			};

			vcc1v8_dvp: LDO_REG1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcc1v8_dvp";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc3v0_tp: LDO_REG2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3000000>;
				regulator-max-microvolt = <3000000>;
				regulator-name = "vcc3v0_tp";
				regulator-state-mem {
					//regulator-off-in-suspend;
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <3000000>;
				};
			};

			vcc1v8_pmu: LDO_REG3 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcc1v8_pmu";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1800000>;
				};
			};

			vccio_sd: LDO_REG4 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3000000>;
				regulator-name = "vccio_sd";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <3000000>;
				};
			};

			vcca3v0_codec: LDO_REG5 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3000000>;
				regulator-max-microvolt = <3000000>;
				regulator-name = "vcca3v0_codec";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_1v5: LDO_REG6 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1500000>;
				regulator-max-microvolt = <1500000>;
				regulator-name = "vcc_1v5";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1500000>;
				};
			};

			vcca1v8_codec: LDO_REG7 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcca1v8_codec";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_3v0: LDO_REG8 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3000000>;
				regulator-max-microvolt = <3000000>;
				regulator-name = "vcc_3v0";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <3000000>;
				};
			};

			vcc3v3_s3: SWITCH_REG1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-name = "vcc3v3_s3";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc3v3_s0: SWITCH_REG2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-name = "vcc3v3_s0";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};
		};
	};
};

&i2c4 {
	status = "okay";
	i2c-scl-rising-time-ns = <475>;
	i2c-scl-falling-time-ns = <26>;

};

&i2c5 {
	status = "disabled";
	i2c-scl-rising-time-ns = <150>;
	i2c-scl-falling-time-ns = <30>;
	clock-frequency = <400000>;	
};


&io_domains {
	status = "okay";

	bt656-supply = <&vcc_1v8>;		/* bt656_gpio2ab_ms */
	audio-supply = <&vcca1v8_codec>;	/* audio_gpio3d4a_ms */
	sdmmc-supply = <&vccio_sd>;		/* sdmmc_gpio4b_ms */
	gpio1830-supply = <&vcc_3v0>;		/* gpio1833_gpio4cd_ms */
};

&pcie_phy {
	status = "okay";
};

&pcie0 {
	ep-gpios = <&gpio0 13 GPIO_ACTIVE_HIGH>;
	num-lanes = <4>;
	pinctrl-names = "default";
	pinctrl-0 = <&pcie_clkreqn_cpm>;
	phy-supply = <&vcc3v3_pcie>;
	status = "okay";
};

&pmu_io_domains {
	status = "okay";
	pmu1830-supply = <&vcc_1v8>;
};

&pinctrl {
	pmic {
		pmic_int_l: pmic-int-l {
			rockchip,pins =
				<1 21 RK_FUNC_GPIO &pcfg_pull_up>;
		};
		
//bill no
		pmic_dvs2: pmic-dvs2 {
			rockchip,pins =
				<1 5 RK_FUNC_GPIO &pcfg_pull_down>;
		};
		
		vsel1_gpio: vsel1-gpio {
			rockchip,pins =
				<1 17 RK_FUNC_GPIO &pcfg_pull_down>;
		};

		vsel2_gpio: vsel2-gpio {
			rockchip,pins =
				<1 14 RK_FUNC_GPIO &pcfg_pull_down>;
		};
	};
	
	pcie {
		pcie_drv: pcie-drv {
			rockchip,pins =
				<1 20 RK_FUNC_GPIO &pcfg_pull_none>;
			};
		//pcie_3g_drv: pcie-3g-drv {
		//	rockchip,pins =
		//		<0 2 RK_FUNC_GPIO &pcfg_pull_up>;
		//};
	};	


	sdio-pwrseq {
		wifi_enable_h: wifi-enable-h {
			rockchip,pins =
				<0 10 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	wireless-bluetooth {
		uart0_gpios: uart0-gpios {
			rockchip,pins =
				<2 19 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
	
	vcc_sd {
		vcc_sd_h: vcc-sd-h {
			rockchip,pins =
				<0 1 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};
};



&rkvdec {
	status = "okay";
};



&rockchip_suspend {
	status = "okay";
	rockchip,sleep-debug-en = <0>;
	rockchip,sleep-mode-config = <
		(0
		| RKPM_SLP_ARMPD
		| RKPM_SLP_PERILPPD
		| RKPM_SLP_DDR_RET
		| RKPM_SLP_PLLPD
		| RKPM_SLP_CENTER_PD
		| RKPM_SLP_AP_PWROFF
		)
		>;
	rockchip,wakeup-config = <
		(0
		| RKPM_GPIO_WKUP_EN
		| RKPM_PWM_WKUP_EN
		)
		>;
		rockchip,pwm-regulator-config = <
		(0
		| PWM2_REGULATOR_EN
		)
		>;
	rockchip,power-ctrl =
		<&gpio1 17 GPIO_ACTIVE_HIGH>,
		<&gpio1 14 GPIO_ACTIVE_HIGH>;
	
};

&saradc {
	status = "okay";
};

&sdhci {
	bus-width = <8>;
	keep-power-in-suspend;
	mmc-hs400-1_8v;
	mmc-hs400-enhanced-strobe;
	non-removable;
	status = "okay";
	supports-emmc;
};
&sdmmc {
	clock-frequency = <150000000>;
	clock-freq-min-max = <100000 150000000>;
	supports-sd;
	bus-width = <4>;
	cap-mmc-highspeed;
	cap-sd-highspeed;
	disable-wp;
	num-slots = <1>;
	//sd-uhs-sdr104;
	vmmc-supply = <&vcc_sd>;
	vqmmc-supply = <&vccio_sd>;
	pinctrl-names = "default";
	pinctrl-0 = <&sdmmc_clk &sdmmc_cmd &sdmmc_cd &sdmmc_bus4>;
	status = "okay";
};

&sdio0 {
	clock-frequency = <50000000>;
	clock-freq-min-max = <200000 50000000>;
	supports-sdio;
	bus-width = <4>;
	disable-wp;
	cap-sd-highspeed;
	cap-sdio-irq;
	keep-power-in-suspend;
	mmc-pwrseq = <&sdio_pwrseq>;
	non-removable;
	num-slots = <1>;
	pinctrl-names = "default";
	pinctrl-0 = <&sdio0_bus4 &sdio0_cmd &sdio0_clk>;
	sd-uhs-sdr104;
	status = "okay";
};

&spdif {
	status = "okay";
	pinctrl-0 = <&spdif_bus>;
	i2c-scl-rising-time-ns = <450>;
	i2c-scl-falling-time-ns = <15>;
	#sound-dai-cells = <0>;
};

&tcphy0 {
	status = "okay";
};

&tcphy1 {
	status = "okay";
};




&u2phy0 {
	status = "okay";

	u2phy0_otg: otg-port {
		status = "okay";
	};

	u2phy0_host: host-port {
		status = "okay";
	};
};

&u2phy1 {
	status = "okay";

	u2phy1_otg: otg-port {
		status = "okay";
	};

	u2phy1_host: host-port {
		status = "okay";
	};
};

&spi1 {
//	status = "disabled";
	status = "okay";
        spi_wk2xxx: spi_wk2xxx@00{
                status = "okay";
                compatible = "spi-wk2xxx";
                reg = <0>;
                spi-max-frequency = <10000000>;
                reset-gpio = <&gpio4 29 GPIO_ACTIVE_HIGH>;
                irq-gpio = <&gpio0 8 IRQ_TYPE_EDGE_FALLING>;
                cs-gpio = <&gpio1 10 GPIO_ACTIVE_HIGH>;
        };

};
&usbdrd3_0 {
	status = "okay";

};

&usbdrd3_1 {
	status = "okay";
};

&usbdrd_dwc3_0 {
	usb3-power-gpio = <&gpio4 26 GPIO_ACTIVE_HIGH>;
	dr_mode = "host";
	status = "okay";
};

&usbdrd_dwc3_1 {
	status = "okay";
	dr_mode = "host";
};
&usb_host0_ehci {
	status = "okay";
};

&usb_host0_ohci {
	status = "okay";
};

&usb_host1_ehci {
	status = "okay";
};

&usb_host1_ohci {
	status = "okay";
};


&pvtm {
	status = "okay";
};

&pmu_pvtm {
	status = "okay";
};

&vpu {
	status = "okay";
};

&isp0 {
	status = "okay";
};

&isp0_mmu {
	status = "okay";
};

&isp1 {
	status = "okay";
};

&isp1_mmu {
	status = "okay";
};

&iep {
	status = "okay";
};

&iep_mmu {
	status = "okay";
};

