
/*
 * Copyright (c) 2016 Fuzhou Rockchip Electronics Co., Ltd
 *
 * This file is dual-licensed: you can use it either under the terms
 * of the GPL or the X11 license, at your option. Note that this dual
 * licensing only applies to this file, and not this project as a
 * whole.
 *
 *  a) This file is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation; either version 2 of the
 *     License, or (at your option) any later version.
 *
 *     This file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 * Or, alternatively,
 *
 *  b) Permission is hereby granted, free of charge, to any person
 *     obtaining a copy of this software and associated documentation
 *     files (the "Software"), to deal in the Software without
 *     restriction, including without limitation the rights to use,
 *     copy, modify, merge, publish, distribute, sublicense, and/or
 *     sell copies of the Software, and to permit persons to whom the
 *     Software is furnished to do so, subject to the following
 *     conditions:
 *
 *     The above copyright notice and this permission notice shall be
 *     included in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *     OTHER DEALINGS IN THE SOFTWARE.
 */

/dts-v1/;

#include "rk3399-excavator-sapphire.dtsi"
#include "rk3399-android.dtsi"
#include "rk3399-vop-clk-set.dtsi"

/ {
	compatible = "rockchip,android", "rockchip,rk3399";

		backlight: backlight {
		//status = "disabled";
		status = "okay";
		compatible = "pwm-backlight";
		pwms = <&pwm0 0 25000 0>;
		brightness-levels = <
			  0   1   2   3   4   5   6   7
			  8   9  10  11  12  13  14  15
			 16  17  18  19  20  21  22  23
			 24  25  26  27  28  29  30  31
			 32  33  34  35  36  37  38  39
			 40  41  42  43  44  45  46  47
			 48  49  50  51  52  53  54  55
			 56  57  58  59  60  61  62  63
			 64  65  66  67  68  69  70  71
			 72  73  74  75  76  77  78  79
			 80  81  82  83  84  85  86  87
			 88  89  90  91  92  93  94  95
			 96  97  98  99 100 101 102 103
			104 105 106 107 108 109 110 111
			112 113 114 115 116 117 118 119
			120 121 122 123 124 125 126 127
			128 129 130 131 132 133 134 135
			136 137 138 139 140 141 142 143
			144 145 146 147 148 149 150 151
			152 153 154 155 156 157 158 159
			160 161 162 163 164 165 166 167
			168 169 170 171 172 173 174 175
			176 177 178 179 180 181 182 183
			184 185 186 187 188 189 190 191
			192 193 194 195 196 197 198 199
			200 201 202 203 204 205 206 207
			208 209 210 211 212 213 214 215
			216 217 218 219 220 221 222 223
			224 225 226 227 228 229 230 231
			232 233 234 235 236 237 238 239
			240 241 242 243 244 245 246 247
			248 249 250 251 252 253 254 255>;
		default-brightness-level = <255>;
//		enable-gpios = <&gpio2 28 GPIO_ACTIVE_HIGH>;
	};
	led-gpio {
		compatible = "led-led";
		breathe-led = <&gpio4 24 GPIO_ACTIVE_HIGH>;
	};
	
	5v_en {
		compatible = "5v-en-gpio";
		power_en-gpio = <&gpio4 30 GPIO_ACTIVE_HIGH>;
		gsm_power_en = <&gpio4 22 GPIO_ACTIVE_HIGH>;
		uart5_en-gpio = <&gpio4 29 GPIO_ACTIVE_HIGH>;
		hub_rst = <&gpio1 24 GPIO_ACTIVE_HIGH>;
		fan_open = <&gpio1 18 GPIO_ACTIVE_HIGH>;
		rp_not_deep_leep = <0>;
		status = "okay";
	};
	
	gpio_ctl{
		compatible = "gpio_ctl";
		base_value = <1000>; //3288_5.1 = 0   3288_7.1.2 = 992  3288_ubuntu = 992   3399_7.1.2 = 1000 3399_ubuntu = 0
		gpio_ctl_num1 = <&gpio4 21 GPIO_ACTIVE_LOW>;

	};

	//vcc_lcd: vcc-lcd {
	//	compatible = "regulator-fixed";
	//	regulator-name = "vcc_lcd";
	//	//gpio = <&gpio4 30 GPIO_ACTIVE_HIGH>;
	//	startup-delay-us = <20000>;
	//	enable-active-high;
	//	regulator-min-microvolt = <3300000>;
	//	regulator-max-microvolt = <3300000>;
	//	regulator-boot-on;
	//	vin-supply = <&vcc5v0_sys>;
	//};
	

	panel: panel {
		status = "okay";
		compatible = "simple-panel";
		backlight = <&backlight>;
		power-supply = <&vcc3v3_sys>;
		enable-gpios = <&gpio1 13 GPIO_ACTIVE_HIGH>;
		prepare-delay-ms = <20>;
		enable-delay-ms = <20>;
		reset  = <&gpio1 0 GPIO_ACTIVE_HIGH>;
		reset-delay-ms = <200>;

		display-timings {
			native-mode = <&timing1>;

			timing0: timing0 {//EDP 15.5
				clock-frequency = <138000000>;
				hactive = <1920>;
				vactive = <1080>;
				hfront-porch = <48>;
				hsync-len = <32>;
				hback-porch = <80>;
				vfront-porch = <3>;
				vsync-len = <5>;
				vback-porch = <23>;
				hsync-active = <0>;
				vsync-active = <0>;
				de-active = <0>;
				pixelclk-active = <0>;
			};
			timing1: timing1 {//EDP 13.3
				clock-frequency = <150000000>;
				hactive = <1920>;
				vactive = <1080>;
				hfront-porch = <12>;
				hsync-len = <16>;
				hback-porch = <48>;
				vfront-porch = <8>;
				vsync-len = <4>;
				vback-porch = <8>;
				hsync-active = <0>;
				vsync-active = <0>;
				de-active = <0>;
				pixelclk-active = <0>;
			};
		};

		ports {
			panel_in: endpoint {
				remote-endpoint = <&edp_out>;
			};
		};
	};

//	hdmiin-sound {
//		compatible = "rockchip,rockchip-rt5651-tc358749x-sound";
//		rockchip,cpu = <&i2s0>;
//		rockchip,codec = <&rt5651 &rt5651 &tc358749x>;
//		status = "okay";
//	};
};
&pwm0 {
	status = "okay";
};

&edp {
	status = "okay";
	force-hpd;

	ports {
		port@1 {
			reg = <1>;

			edp_out: endpoint {
				remote-endpoint = <&panel_in>;
			};
		};
	};
};

&edp_in_vopl {
	status = "disabled";
};

&hdmi {
	status = "okay";
};

&hdmi_in_vopb {
	status = "disabled";
};

//&rt5651 {
//	status = "okay";
//};
//
//&cdn_dp {
//	status = "okay";
//	extcon = <&fusb0>;
//	phys = <&tcphy0_dp>;
//};
//
//&hdmi_dp_sound {
//	status = "okay";
//};
//
//&dp_in_vopb {
//	status = "disabled";
//};

//&i2c1 {
//	status = "okay";
//
//	gsl3673: gsl3673@40 {
//		compatible = "GSL,GSL3673";
//		reg = <0x40>;
//		screen_max_x = <1536>;
//		screen_max_y = <2048>;
//		irq_gpio_number = <&gpio1 20 IRQ_TYPE_LEVEL_LOW>;
//		rst_gpio_number = <&gpio4 22 GPIO_ACTIVE_HIGH>;
//	};

//	tc358749x: tc358749x@0f {
//		compatible = "toshiba,tc358749x";
//		reg = <0x0f>;
//		power-gpios = <&gpio2 6 GPIO_ACTIVE_HIGH>;
//		power18-gpios = <&gpio2 9 GPIO_ACTIVE_HIGH>;
//		power33-gpios = <&gpio2 5 GPIO_ACTIVE_HIGH>;
//		csi-ctl-gpios = <&gpio2 10 GPIO_ACTIVE_HIGH>;
//		stanby-gpios = <&gpio2 8 GPIO_ACTIVE_HIGH>;
//		reset-gpios = <&gpio2 7 GPIO_ACTIVE_HIGH>;
//		int-gpios = <&gpio2 12 GPIO_ACTIVE_HIGH>;
//		pinctrl-names = "default";
//		pinctrl-0 = <&hdmiin_gpios>;
//		status = "okay";
//	};
//};

//&i2c6 {
//	cw2015@62 {
//		status = "disabled";
//		compatible = "cw201x";
//		reg = <0x62>;
//		bat_config_info = <0x15 0x42 0x60 0x59 0x52 0x58 0x4D 0x48
//				   0x48 0x44 0x44 0x46 0x49 0x48 0x32 0x24
//				   0x20 0x17 0x13 0x0F 0x19 0x3E 0x51 0x45
//				   0x08 0x76 0x0B 0x85 0x0E 0x1C 0x2E 0x3E
//				   0x4D 0x52 0x52 0x57 0x3D 0x1B 0x6A 0x2D
//				   0x25 0x43 0x52 0x87 0x8F 0x91 0x94 0x52
//				   0x82 0x8C 0x92 0x96 0xFF 0x7B 0xBB 0xCB
//				   0x2F 0x7D 0x72 0xA5 0xB5 0xC1 0x46 0xAE>;
//		monitor_sec = <5>;
//		virtual_power = <0>;
//	};
//};

&isp0 {
	status = "okay";
};

&isp1 {
	status = "okay";
};

&isp0_mmu {
	status = "okay";
};

&isp1_mmu {
	status = "okay";
};

&vopb {
	status = "okay";
	assigned-clocks = <&cru DCLK_VOP0_DIV>;
	assigned-clock-parents = <&cru PLL_CPLL>;
};
&vopb_mmu {
        status = "okay";
};

&vopl {
	status = "okay";
	assigned-clocks = <&cru DCLK_VOP1_DIV>;
	assigned-clock-parents = <&cru PLL_VPLL>;
};
&vopl_mmu {
        status = "okay";
};

//&pcie_phy {
//	status = "okay";
//};
//
//&pcie0 {
//	status = "okay";
//};

&route_edp {
	status = "okay";
};
 
//&route_hdmi { 
//        status = "okay"; 
//        //status = "disabled"; 
//}; 

&pinctrl {
//	lcd-panel {
//		lcd_panel_reset: lcd-panel-reset {
//			rockchip,pins = <1 0 RK_FUNC_GPIO &pcfg_pull_up>;
//		};
//	};

//	hdmiin {
//		hdmiin_gpios: hdmiin_gpios {
//		rockchip,pins =
//				<2 5 RK_FUNC_GPIO &pcfg_pull_none>,
//				<2 6 RK_FUNC_GPIO &pcfg_pull_none>,
//				<2 7 RK_FUNC_GPIO &pcfg_pull_none>,
//				<2 8 RK_FUNC_GPIO &pcfg_pull_none>,
//				<2 9 RK_FUNC_GPIO &pcfg_pull_none>,
//				<2 12 RK_FUNC_GPIO &pcfg_pull_none>;
//		};
//	};
};

